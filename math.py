import numpy as np
from numpy.linalg import inv, eigvals, eig
from sympy import *
import itertools
import sys
import math



A = np.matrix('1, 1; 1, 2')
B = np.matrix('2, 1; 1, 1')
A_inv = inv(A)
B_inv = inv(B)


def mult_list_of_matrices(matrices):
    result = matrices[-1]
    for i in range(len(matrices) - 2, -1, -1):
        result = np.dot(matrices[i], result)

    return result


def filter_word(word, word_num):
    if np.array_equal(word_num, np.identity(2)):
        return False
    if not abs(np.trace(word_num)) == 2:
        return False

    if "Ainv-A" in word or \
       "A-Ainv" in word or \
       "Binv-B" in word or \
       "B-Binv" in word:
       return False

    return True


def compute_wanted_words_and_eigenvalues(n):
    matrices_names = ['A', 'B', 'Ainv', 'Binv']
    matrices = [A, B, A_inv, B_inv]

    matrices_names_list = [matrices_names for _ in range(n)]
    matrices_list = [matrices for _ in range(n)]

    all_words = list(itertools.product(*matrices_names_list))
    all_words = ['-'.join(word) for word in all_words]
    all_matrices_combinations  = list(itertools.product(*matrices_list))

    all_words_num = [mult_list_of_matrices(combination) for combination in all_matrices_combinations]

    wanted_words = {word: {"word_num" : word_num, "trace": np.trace(word_num)} for (word, word_num) in zip(all_words, all_words_num) if filter_word(word, word_num)}
    return wanted_words


def p_print_dict(dict):
    for (key, value) in dict.items():
        print(f"Word '{key}' : {value}")


def p_print_list(list):
    [print(x) for x in list]


def find_eigenvect_in_z(word):
    M = Matrix(word.astype(int))
    evec_in_z = M.eigenvects(simplify=True)[0][2][0]

    return evec_in_z


def compute_wanted_vector(word, evec_in_z):
    print(word)
    v = evec_in_z
    word = word.split('-')
    sum = -v
    temp = v
    print(temp)
    stop_letter = 'A' if 'A' in word[-1] else 'B'

    for i in range(len(word)-1, -1, -1):
        if stop_letter not in ''.join(word[0:i+1]):
            break

        current_mat = word[i]
        if current_mat == 'A':
            temp = A*temp
        elif current_mat == 'Ainv':
            temp = A_inv*temp
        elif current_mat == 'B':
            temp = B*temp
        elif current_mat == 'Binv':
            temp = B_inv*temp
        if i % 2 == 0:
            sum = sum - temp
        else:
            sum = sum + temp
        print(temp)

    print(sum)
    return sum


def get_wanted_vector(words_and_eigenvalues):

    evecs_in_z = [find_eigenvect_in_z(value["word_num"]) for value in words_and_eigenvalues.values()]

    for (word_and_eigenvalues, i) in zip(words_and_eigenvalues.values(), range(len(evecs_in_z))):
        word_and_eigenvalues["evec_in_z"] = evecs_in_z[i]

    computed_wanted_vectors = [compute_wanted_vector(k, v["evec_in_z"].copy()) for (k, v) in words_and_eigenvalues.items()]
    # p_print_list(computed_wanted_vectors)

    for (word_and_eigenvalues, i) in zip(words_and_eigenvalues.values(), range(len(computed_wanted_vectors))):
        word_and_eigenvalues["result"] = computed_wanted_vectors[i]
    # p_print_dict(words_and_eigenvalues)

    return words_and_eigenvalues


def find_wanted_pairs(words_and_values):
    words = [k for k in words_and_values.keys()]
    word_pairs = list(itertools.permutations(words, 2))

    wanted_word_pairs = []
    for word_pair in word_pairs:
        vec1 = words_and_values[word_pair[0]]["result"]
        vec2 = words_and_values[word_pair[1]]["result"]
        M = Matrix([[vec1[0], vec2[0]], [vec1[1], vec2[1]]])

        if abs(M.det()) == 1:
            print("---------------------")
            print(word_pair)
            print(M)

            wanted_word_pairs.append(word_pair)

    return wanted_word_pairs


def main(n):
    wanted_words_and_eigenvalues = compute_wanted_words_and_eigenvalues(n)
    # print(f"Found {len(wanted_words_and_eigenvalues)} words that have either 1 or -1 as an eigenvalue")
    # p_print_dict(wanted_words_and_eigenvalues)

    words_and_values = get_wanted_vector(wanted_words_and_eigenvalues)
    p_print_dict(words_and_values)

    # for k, v in words_and_values.items():
    #     print("Word : ", k)
    #     print("corresponding vector : ", v["result"])
    #     print()

    wanted_pairs = find_wanted_pairs(words_and_values)

    return wanted_pairs



if __name__ == "__main__":
    # n = 0
    # try:
    #     n = int(sys.argv[1])
    #     if n < 2:
    #         raise ValueError()
    # except ValueError:
    #     raise Exception("The n provided to the program is not a number OR is smaller than 2")
    # except:
    #     raise Exception("No n provided to the program")

    # for n in range(2, 11):
    for n in range(6, 7):
        print(f"------------------------- {n} -------------------------")
        wanted_pairs = main(n)
        # print(wanted_pairs)
        